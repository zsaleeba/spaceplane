use std::net::TcpStream;
use std::sync::mpsc;

struct MavlinkTcp {
    stream: TcpStream,

    // Send channel.
    send_channel_tx: mpsc::Sender<Vec<u8>>,
    send_channel_rx: mpsc::Receiver<Vec<u8>>,

    // Callback for received data.
    recv_callback: fn(&MavlinkTcp, &[u8]),
}

impl MavlinkTcp {
    // Where to find the MAVlink host.
    const host: &str = "127.0.0.1";
    const port: u32 = 5760;

    // Create a new TCP connection to the simulated autopilot.
    pub fn new(recv_callback: fn(&MavlinkTcp, &[u8])) -> MavlinkTcp {
        match TcpStream::connect("{}:{}", host, port) {
            Ok(stream) => {
                println!("connected to MAVlink on port {}", port);

                let (stx, srx) = mpsc::channel();

                // Start a thread to read from the TCP stream.
                thread::spawn(recv_thread(&self));
                
                // Start a thread to write to the TCP stream.
                thread::spawn(send_thread(&self));

                MavlinkTcp {
                    stream: stream,
                    send_channel_tx: stx,
                    send_channel_rx: srx,
                    recv_callback: recv_callback,
                }
            }
            Err(e) => {
                println!("couldn't connect to MAVlink port {}: {}", port, e);
                panic!("couldn't connect to MAVlink port {}", port);
            }
        }

    }
    
    // Write to the send channel.
    pub fn write(&mut self, data: &[u8]) {
        match self.send_channel_tx.send(data.to_vec()) {
            Ok(_) => {}
            Err(e) => {
                println!("MAVlink send channel closed");
            }
        }
    }

    // Receive thread function.
    fn recv_thread(&self) {
        loop {
            let mut buf = [0; 1024];
            match self.stream.read(&mut buf).unwrap() {
                0 => {
                    break;
                }
                n => {
                    // Call the callback function with the received data.
                    (self.recv_callback)(&buf[..n]);
                }
            }
        }

        println!("MAVlink connection closed on port {} - read thread quitting", port)
    }

    // Send thread function.
    fn send_thread(&self) {
        loop {
            // Wait until we're notified of new data being available.
            match self.send_channel_rx.recv() {
                Ok(data) => {
                    self.stream.write(&data).unwrap();
                }
                Err(e) => {
                    break;
                }
            }
        }
        println!("MAVlink connection closed on port {} - send thread quitting", port);
    }
}
