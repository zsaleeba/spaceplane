//
// Autonomous mission controller for the spaceplane project.
//
// This program runs on a raspberry pi and communicates with the autopilot 
// using the MAVlink protocol. It is responsible for the high level control
// of the spaceplane.
//
// The mission has five main phases:
//
//   1. Ascent - in this phase the mission controller monitors the 
//      accelerometers, GPS and baro sensors to determine when a balloon
//      has burst. When a balloon has burst the spaceplane is released 
//      from the balloon train and switches to the pullout phase. 
//      Alternatively a manual command may trigger the release.
//
//   2. Pullout - the purpose of this phase is to stabilise the spaceplane
//      after release and reach the high speed necessary to fly in the upper 
//      atmosphere.
//      In this phase the spaceplane flips from a nose up orientation
//      to a nose down dive and descends through the low pressure atmosphere 
//      while gaining speed rapidly. When the spaceplane has stabilised the 
//      mission controller performs three functions in parallel:
//      a. It controls to pitch to allow the speed to increase to the target
//         flying speed for its altitude. This will be around 500kmh at 30km 
//         altitude and reduces as it descends.
//      b. It also prevents the speeds from becoming high enough to damage the 
//         spaceplane. This is achieved by pulling out of the dive in a
//         controlled way.
//      c. It sets a target heading to start the spaceplane moving towards the
//         landing location.
//    
//   3. Return to launch - in this phase the spaceplane glides towards the 
//      landing location while maintaining an optimal distance-covering 
//      speed. This is achieved through fine pitch adjustments.
//
//   4. Descent - when it's above the landing location the spaceplane
//      starts circling to maintain its ground location while scrubbing off 
//      altitude. Split elevon air braking may be used to aid in the descent
//      process.
//
//   5. Landing - when the spaceplane has descended to within a short distance 
//      of the ground the ground crew take over control and land the
//      spaceplane manually.
//
//
// This software system is designed to run on a raspberry pi connected to the
// autopilot via a serial connection using the MAVlink protocol. For 
// simulation purposes it can be connected via TCP port 5760 to the simulated 
// ardupilot instead.
// 
// The system act in place of a GCS (ground control system) and commands the 
// ardupilot via high level commands while monitoring the spaceplane's sensors 
// and telemetry data.
//
// It can also receive commands from the ground crew via satellite or cellular
// communications. These commands will direct the mission controller's actions.
//


//
// Program structure:
//
//  * Mission controller  - determines the action to take given the information
//                          available
//  * Sensor handler      - interprets sensor messages from the autopilot
//  * Comms gateway       - directs selected messages to and from the cellular
//                          and satellite links
//  * Cellular comms      - communicates selected MAVlink data via the cellular link
//  * Satellite comms     - communicates selected MAVlink data via the satellite link
//  * Serial telemetry    - communicates mavlink messages to and from the autopilot
//  * TCP telemetry       - communicates mavlink messages to and from a simulated
//                          autopilot
//

use mavlink;
use std::{thread, time};

fn main() {
    println!("space_mission v0.1.0 starting");

    // Create our mavlink header and heartbeat message
    let header = mavlink_header();
    let heartbeat = mavlink_heartbeat_message();

    // Main loop
    loop {
        // Write the mavlink message via serial
        mavlink::write_versioned_msg(&mut tx, mavlink::MavlinkVersion::V2, header, &heartbeat)
            .unwrap();

        // Delay for 1 second
        thread::sleep(time::Duration::from_millis(1000));
    }
}

fn mavlink_header() -> mavlink::MavHeader {
    mavlink::MavHeader {
        system_id: 1,
        component_id: 1,
        sequence: 42,
    }
}

pub fn mavlink_heartbeat_message() -> mavlink::common::MavMessage {
    mavlink::common::MavMessage::HEARTBEAT(mavlink::common::HEARTBEAT_DATA {
        custom_mode: 0,
        mavtype: mavlink::common::MavType::MAV_TYPE_SUBMARINE,
        autopilot: mavlink::common::MavAutopilot::MAV_AUTOPILOT_ARDUPILOTMEGA,
        base_mode: mavlink::common::MavModeFlag::empty(),
        system_status: mavlink::common::MavState::MAV_STATE_STANDBY,
        mavlink_version: 0x3,
    })
}
